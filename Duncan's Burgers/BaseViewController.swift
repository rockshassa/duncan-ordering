//
//  BaseViewController.swift
//  Duncan's Burgers
//
//  Created by Nicholas Galasso on 10/18/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    @IBOutlet weak var orderButton: UIButton!
    @IBOutlet weak var duncansLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        duncansLabel.sizeToFit()
        
        self.view.backgroundColor = Colors.backgroundColor()
        
        orderButton.addTarget(self, action: "tappedOrder:", forControlEvents: .TouchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        duncansLabel.center = CGPoint(x: self.view.center.x, y:120)
    }

    func tappedOrder(sender:UIButton){
        let vc = OrderViewController()
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.barTintColor = Colors.navBarColor()
        
        self.presentViewController(nav, animated: true) { () -> Void in
            Mixpanel.sharedInstance().track("opened order screen")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
