//
//  MenuItem.swift
//  Duncan's Burgers
//
//  Created by Nicholas Galasso on 10/18/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import UIKit

class MenuItem {
    
    var name = ""
    var description = ""
    var uniqueID = ""
    
    init(uniqueID:String, name:String, description:String) {
        self.uniqueID = uniqueID
        self.name = name
        self.description = description
    }
    
    static func copyItem(item:MenuItem) -> MenuItem {
        return MenuItem(uniqueID: item.uniqueID, name: item.name, description: item.description)
    }
    
    func toEmail() -> String {
        return "\(self.name)"
    }
    
}
