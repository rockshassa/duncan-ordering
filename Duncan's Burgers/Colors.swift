//
//  Colors.swift
//  Duncan's Burgers
//
//  Created by Nicholas Galasso on 10/26/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import Foundation

class Colors {
    
    static func color(r:CGFloat, g:CGFloat, b:CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    static func backgroundColor() -> UIColor {
        return Colors.color(115, g: 40, b: 40)
    }
    
    static func navBarColor() -> UIColor {
        return Colors.color(60, g: 40, b: 40)
    }
}