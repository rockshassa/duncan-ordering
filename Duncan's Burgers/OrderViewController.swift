//
//  OrderViewController.swift
//  Duncan's Burgers
//
//  Created by Nicholas Galasso on 10/18/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import UIKit
import MessageUI

class OrderViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MFMailComposeViewControllerDelegate {

    let orderKeyPath = "saved_order_count"
    
    var orderNumber = 0
    
    let data:[MenuItem] = BizLogic.menu()
    var order:[MenuItem] = []
    var name:String? {
        didSet {
            if let n = name {
                self.title = "\(n)'s order - #\(orderNumber)"
            }
        }
    }
    
    var notes:String? {
        didSet {
            if let n = notes {
                self.notesLabel.text = "Notes: \(n)"
            }
        }
    }
    
    let collectionViewWidth = NSNumber(int: 280)
    
    var menuCollectionView:UICollectionView!
    var orderCollectionView:UICollectionView!
    let emailButton = UIButton()
    let menuLabel = UILabel()
    let orderLabel = UILabel()
    let notesButton = UIButton(type: .RoundedRect)
    let nameButton = UIButton(type: .RoundedRect)
    let notesLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.lightTextColor()]
        self.view.backgroundColor = Colors.backgroundColor()
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.menuCollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout:UICollectionViewFlowLayout() )
        self.orderCollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout:UICollectionViewFlowLayout() )
        
        for view in [menuCollectionView,orderCollectionView,emailButton,menuLabel,orderLabel,notesButton,nameButton,notesLabel] {
            self.view.addSubview(view)
        }
        
        notesButton.setTitle("Add Notes", forState: .Normal)
        notesButton.addTarget(self, action: "tappedNotes:", forControlEvents: .TouchUpInside)
        nameButton.setTitle("Edit Name", forState: .Normal)
        nameButton.addTarget(self, action: "tappedName:", forControlEvents: .TouchUpInside)
        
        for btn in [notesButton, nameButton]{
            btn.backgroundColor = UIColor.clearColor()
            btn.sizeToFit()
        }
        
        let dismissBtn = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: "tappedDismiss:")
        self.navigationItem.leftBarButtonItem = dismissBtn
        
        let doneBtn = UIBarButtonItem(title: "Send Order", style: .Done, target: self, action: "tappedEmail:")
        self.navigationItem.rightBarButtonItem = doneBtn
        
        self.menuLabel.text = "Duncan's Menu"
        self.orderLabel.text = "This Order"
//        self.notesLabel.text = "Notes:"
        
        for label in [self.orderLabel, self.menuLabel, self.notesLabel] {
            label.sizeToFit()
            label.backgroundColor = UIColor.clearColor()
            label.textColor = UIColor.lightTextColor()
        }
        
        for cv in [self.menuCollectionView, self.orderCollectionView] {
            
            cv.registerClass(MenuCollectionViewCell.self, forCellWithReuseIdentifier: MenuCollectionViewCell.reuseIdentifier())
            
            cv.dataSource = self
            cv.delegate = self
            
            cv.backgroundColor = Colors.navBarColor()
        }
        
        self.emailButton.addTarget(self, action: "tappedEmail:", forControlEvents: .TouchUpInside)
        
        self.orderNumber = 1 + NSUserDefaults.standardUserDefaults().integerForKey(self.orderKeyPath)
        
        print("initialized order number \(self.orderNumber)")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let pad = 40
        if let yPos = self.navigationController?.navigationBar.frame.size.height{
            
            self.menuLabel.frame = CGRect(origin: CGPointMake(CGFloat(pad), CGFloat(Int(yPos)+pad)), size: self.menuLabel.frame.size)
            self.orderLabel.frame = CGRect(origin: CGPointMake(CGFloat( (Int(self.view.frame.size.width)-self.collectionViewWidth.integerValue)-pad), CGFloat(Int(yPos)+pad)), size: self.menuLabel.frame.size)
            
            self.nameButton.center = CGPointMake(self.view.center.x, self.view.center.y-40)
            self.notesButton.center = CGPointMake(self.view.center.x, self.view.center.y+40)
        }
        
        self.menuCollectionView.frame = CGRect(x: pad, y: Int(CGRectGetMaxY(self.menuLabel.frame))+pad, width: self.collectionViewWidth.integerValue, height: 440)
        self.orderCollectionView.frame = CGRect(x: (Int(self.view.frame.size.width)-self.collectionViewWidth.integerValue)-pad , y: Int(CGRectGetMaxY(self.menuLabel.frame))+pad, width: self.collectionViewWidth.integerValue, height: 440)
        
        self.notesLabel.frame = CGRect(origin: CGPointZero, size: CGSize(width: 300, height: 200))
        self.notesLabel.center = CGPointMake(self.view.center.x, CGRectGetMaxY(self.notesButton.frame)+100)
//        self.notesLabel.backgroundColor = UIColor.whiteColor()
        self.notesLabel.numberOfLines = 0
        
        self.menuCollectionView.reloadData()
        self.orderCollectionView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.askForName()
    }
    
    func tappedDismiss(sender:UIBarButtonItem){
        self.navigationController?.presentingViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    func tappedNotes(sender:UIButton){
        self.askForNotes()
    }

    func tappedEmail(sender:UIButton){
        
        if (self.order.count == 0){
            let ac = UIAlertController(title: "Empty Order", message:nil, preferredStyle: .Alert)
            ac.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in
                
            }))
            self.presentViewController(ac, animated: true, completion: { () -> Void in
                
            })
            return
        }
        
        if MFMailComposeViewController.canSendMail() {
            let controller = MFMailComposeViewController()
            
            controller.setSubject(self.emailSubjectString(self.order) as String)
            controller.setToRecipients([BizLogic.businessEmail()])
            controller.setMessageBody(self.stringFromOrder(self.order), isHTML: false)
            controller.mailComposeDelegate = self
            
            self.presentViewController(controller, animated: true, completion: { () -> Void in
                
            })
        }
        
    }
    
    func tappedName(sender:AnyObject){
        self.askForName()
    }
    
    func askForName(){
        
        let avc = UIAlertController(title: "Enter customer name", message: nil, preferredStyle: .Alert)
        
        avc.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Name"
        }
        
        let doneAction = UIAlertAction(title: "OK", style: .Default) { (action) -> Void in
            
            let nameField = avc.textFields![0] as UITextField
            self.name = nameField.text
        }
        
        avc.addAction(doneAction)
        
        self.presentViewController(avc, animated: true) { () -> Void in
            
        }
    }
    
    func askForNotes(){
        
        let avc = UIAlertController(title: "Enter order notes", message: nil, preferredStyle: .Alert)
        
        avc.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Notes"
            
            if let n = self.notes {
                textField.text = n
            }
        }
        
        let doneAction = UIAlertAction(title: "Done", style: .Default) { (action) -> Void in
            
            let noteField = avc.textFields![0] as UITextField
            
            self.notes = noteField.text
            
            self.notesButton.setTitle("Edit Notes", forState: .Normal)
        }
        
        avc.addAction(doneAction)
        
        self.presentViewController(avc, animated: true) { () -> Void in
            
        }
    }
    
    func stringFromOrder(orderList:[MenuItem]) -> String {
        
        let emailString = NSMutableString(string:"")
        
        if let name = self.name{
            emailString.appendString("\(name)")
        }
        
        for item in orderList {
            emailString.appendString("\n")
            emailString.appendString(item.toEmail())
        }
        if let notes = self.notes {
            emailString.appendString("\n")
            emailString.appendString("\(notes) ")
        }
        emailString.appendString("\nOrder #\(self.orderNumber)")
        
        return emailString as String
    }
    
    func emailSubjectString(orderList:[MenuItem]) -> String {
        
        var dict:[String:NSNumber] = [:]
        for item in orderList {
            if let count = dict[item.uniqueID] {
                let newCount = count.integerValue + 1
                dict[item.uniqueID] = NSNumber(integer: newCount)
            } else {
                dict[item.uniqueID] = NSNumber(integer: 1)
            }
        }
        
        let s = NSMutableString(string: "")
        
        for (key,object) in dict {
            s.appendString("\(object.stringValue)\(key) ")
        }
        
        if let notes = self.notes {
            s.appendString(" - \(notes) ")
        }
        
        s.appendString(" - #\(self.orderNumber)")
        
        return s as String
    }
    
    func trackingInfoFromOrder(orderList:[MenuItem]) -> [NSObject:AnyObject] {
        
        var properties:[NSObject:AnyObject] = [:]
        
        properties["number"] = "\(self.orderNumber)"
        
        if let n = self.name {
            properties["name"] = n
        }
        
        let items = NSMutableArray()
        
        for item in orderList {
            
            items.addObject(item.uniqueID)
        }
        
        properties["items"] = items
        
        return properties
    }
    
    /*
    MFMailComposeViewController Delegate
    */
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {

        NSUserDefaults.standardUserDefaults().setInteger(self.orderNumber, forKey: self.orderKeyPath)
        
        print(result)
        
        if let e = error {
            print(e.localizedDescription)
        }
        
        if result == MFMailComposeResultSent {
            
            self.trackOrder()
            
            self.presentingViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            })
            
        } else if result == MFMailComposeResultSaved{
            controller.dismissViewControllerAnimated(true) { () -> Void in
                
            }
        } else if result == MFMailComposeResultFailed {
            controller.dismissViewControllerAnimated(true) { () -> Void in
                
            }
        } else if result == MFMailComposeResultCancelled{
            controller.dismissViewControllerAnimated(true) { () -> Void in
                
            }
        }
        
    }
    
    func trackOrder(){
        Mixpanel.sharedInstance().track("Order", properties: self.trackingInfoFromOrder(self.order))
    }
    
    /*
    UICollectionView Data Source
    */
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (collectionView == self.menuCollectionView) {
            return self.data.count
        } else if (collectionView == self.orderCollectionView) {
            return self.order.count
        }
        
        return 0;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell:MenuCollectionViewCell  = (collectionView.dequeueReusableCellWithReuseIdentifier(MenuCollectionViewCell.reuseIdentifier(), forIndexPath: indexPath) as? MenuCollectionViewCell)!
        
        if (collectionView == self.menuCollectionView) {
            
            cell.menuItem = self.data[indexPath.row]
        
        } else if (collectionView == self.orderCollectionView) {
        
            cell.menuItem = self.order[indexPath.row]
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: self.collectionViewWidth.integerValue, height: 100)
    }
    
    /*
    UICollectionView Delegate
    */
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        if (collectionView == self.menuCollectionView) {

            self.order.insert(self.data[indexPath.row], atIndex: 0)
            self.orderCollectionView.insertItemsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)])

        } else if (collectionView == self.orderCollectionView) {
            
            self.order.removeAtIndex(indexPath.row)
            self.orderCollectionView.deleteItemsAtIndexPaths([indexPath])
        }
    }
}


