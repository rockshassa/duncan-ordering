//
//  MenuCollectionViewCell.swift
//  Duncan's Burgers
//
//  Created by Nicholas Galasso on 10/18/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    let titleLabel:UILabel = UILabel()
    
    var menuItem:MenuItem? {
        didSet{
            
            let textColor = UIColor.lightTextColor()
            
            let text = NSMutableAttributedString(string: "")
            
            let uid = NSMutableAttributedString(string: menuItem!.uniqueID,
                                    attributes:[
                                        NSFontAttributeName:UIFont(name: "HelveticaNeue-BoldItalic", size: 26)!,
                                        NSForegroundColorAttributeName:textColor])
            text.appendAttributedString(uid)
            
            let name = NSMutableAttributedString(string: ":\(menuItem!.name)",
                                    attributes:[
                                        NSFontAttributeName:UIFont(name: "HelveticaNeue", size: 20)!,
                                        NSForegroundColorAttributeName:textColor])
            text.appendAttributedString(name)

            text.appendAttributedString(NSAttributedString(string: "\n"))
            
            let desc = NSMutableAttributedString(string: menuItem!.description,
                                    attributes:[
                                        NSFontAttributeName:UIFont(name: "HelveticaNeue-Italic", size: 14)!,
                                        NSForegroundColorAttributeName:textColor])
            text.appendAttributedString(desc)
            
            self.titleLabel.attributedText = text;

        }
    }
    
    class func reuseIdentifier() -> String {
        return NSStringFromClass(MenuCollectionViewCell)
    }
    
    required override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.titleLabel.backgroundColor = UIColor.clearColor()
        self.titleLabel.numberOfLines = 0
        self.titleLabel.textAlignment = .Left
        
        self.backgroundView = UIView()
        self.backgroundView?.backgroundColor = UIColor(red: 178/255, green: 34/255, blue: 34/255, alpha: 1)
        self.contentView.backgroundColor = UIColor.clearColor()
        self.contentView.addSubview(self.titleLabel)
        self.selectedBackgroundView = UIView()
        self.selectedBackgroundView?.backgroundColor = UIColor.lightTextColor()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }
    
    override func layoutSubviews() {
        self.titleLabel.frame = self.contentView.bounds
    }

    
    
    
}
