//
//  BizLogic.swift
//  Duncan's Burgers
//
//  Created by Nicholas Galasso on 10/26/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import Foundation

class BizLogic {
    
    class func businessEmail() -> String{
        return "duncansorders@gmail.com"
    }
    
    class func venueName() -> String {
        return "Bodega"
    }
    
    class func menu() -> [MenuItem] {
        return [
//            MenuItem(uniqueID: "QC", name: "Quick-Serve Cheeseburger", description: "Meat/Cheese/Special Sauce on a Potato Bun"),
//            MenuItem(uniqueID: "C", name: "Classic Cheeseburger", description: "Lettuce/Tomato/Pickle/Special Sauce/Onions served on the side"),
            MenuItem(uniqueID: "D", name: "Duncan Deluxe", description: "Double Meat/Double Cheese\nLettuce/Tomato/Pickle/Special Sauce/Onions served on the side"),
            MenuItem(uniqueID: "V", name: "Classic Veggie", description: "Monk's Meats Veggie Patty with Cheese\nLettuce/Tomato/Pickle/Special Sauce/Onions served on the side")
        ]
    }
}